package datastructure;

import cellular.CellState;

public class CellGrid implements IGrid {
	
	private int cols;
	private int rows;
	private CellState[][] cells;

    public CellGrid(int rows, int columns, CellState initialState) {
		cols = columns;
		this.rows = rows;
		cells = new CellState[columns][];
		for(int i = 0; i < columns; i++) {
			cells[i] = new CellState[rows];
			for(int j = 0; j < rows; j++) {
				cells[i][j] = initialState;
			}
		}
	}

    @Override
    public int numRows() {
        return rows;
    }

    @Override
    public int numColumns() {
        return cols;
    }

    @Override
    public void set(int row, int column, CellState element) {
        cells[column][row] = element;
    }

    @Override
    public CellState get(int row, int column) {
        return cells[column][row];
    }

    @Override
    public IGrid copy() {
    	CellGrid cellGridCopy = new CellGrid(this.rows, this.cols, null);
    	for(int i = 0; i < cols; i++) {
			for(int j = 0; j < rows; j++) {
				cellGridCopy.cells[i][j] = this.cells[i][j];
			}
    	}
        return cellGridCopy;
    }
    
}
